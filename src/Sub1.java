import java.util.ArrayList;

public class Sub1 {

    public static void main(String[] args) {

    }
}

class B extends A{
    private long t;
    private D d;

    public B(long t){
        this.t=t;
        this.d=new D();
    }

    public void b(){
        C c=new C();
        D d=new D();
        d.met1(1);
        System.out.println(C.class);
    }
}

class A{

}
class C{

}

class D implements Y{
    private ArrayList<F> flist=new ArrayList<>();
    private ArrayList<E> elist=new ArrayList<>();

    public void met1(int i){}
}

class E{

    public void met2(){}
}

class F{

}

interface Y{

}


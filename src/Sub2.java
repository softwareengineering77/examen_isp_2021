import javax.swing.*;
import java.awt.event.*;

public class Sub2 extends JFrame {

    JTextField textField;
    JTextField textField2;
    JButton button;


    Sub2() {
        setTitle("Subject no 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(320, 320);
        setVisible(true);
        setLocation(520, 420);
    }

    public void init() {
        this.setLayout(null);
        int width = 70;
        int height = 15;

        button = new JButton("Move");
        button.setBounds(80, 140, 90, height);

        textField = new JTextField(4);
        textField.setBounds(40, 40, 190, height);

        textField2 = new JTextField(15);
        textField2.setBounds(40, 80, 190, height);
        textField2.setEditable(false);


        button.addActionListener(new Action() {
            public void actionPerformed(ActionEvent ae) {
                textField2.setText(textField.getText());
            }
        });

        add(button);
        add(textField);
        add(textField2);
    }

    public static void main(String[] args) {
        new Sub2();
    }

    class Action implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            String name = textField.getText();
            textField2.setText(name);
        }
    }
}